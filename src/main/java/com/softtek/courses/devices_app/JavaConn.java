package com.softtek.course;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JavaConn{
    static void TryConn(String userString, String pswd, String query){
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", userString, pswd)) {

            if (conn != null) {
                System.out.println("Connected to the database!");
                
                PreparedStatement preparedStatement = conn.prepareStatement(query);

                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    long id = resultSet.getLong("COLORID");
                    String name = resultSet.getString("NAME");
                    String hexValue = resultSet.getString("HEXVALUE");
                    System.out.println("ID: " + id + " color's name: " + name + " hex's value: " + hexValue);
                }
            } else {
                System.out.println("Failed to make connection!");
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }   
    }
}
