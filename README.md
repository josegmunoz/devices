This project integrates most of the software development tools used in the 
Softtek Java academy including Git repos, Maven compilation lifecycles, a MySQL 
database, and built a Java App using OOP where a java interface helps obtain 
query results amongst other data manipulation. The goak of this project is to 
mainly use concepts learned within the Academy in an effort to learn workflow 
development, Object Oriented programming theory such as List objects and Streams
to aid the practice.